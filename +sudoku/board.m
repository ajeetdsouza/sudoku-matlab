function board = board(puzzle, boardTitle, varargin)
gridSize = size(puzzle, 1);
subgridSize = sqrt(gridSize);

% create board
board = figure('Name', 'Sudoku', 'IntegerHandle', 'off', 'MenuBar', 'none', varargin{:});
hold on
title(boardTitle)

% draw minor lines
gridLines = 0:gridSize;
gridLines1 = [gridLines; gridLines];
gridLines2 = repmat([0;gridSize], 1, length(gridLines));
plot(gridLines1, gridLines2, 'black')
plot(gridLines2, gridLines1, 'black')

% draw major lines
gridLines = 0:subgridSize:gridSize;
gridLines1 = [gridLines; gridLines];
gridLines2 = repmat([0;gridSize], 1, length(gridLines));
plot(gridLines1, gridLines2, 'black', 'LineWidth', 2)
plot(gridLines2, gridLines1, 'black', 'LineWidth', 2)

% create grid strings
gridStrings = string(puzzle);
gridStrings(gridStrings == "0") = "";

% location of grid strings
[gridStringsX, gridStringsY] = meshgrid(0.5:gridSize-0.5, gridSize-0.5:-1:0.5);

% draw grid strings
text(gridStringsX(:), gridStringsY(:), gridStrings, 'HorizontalAlignment', 'center')

% styling
axis image off
hold off
end