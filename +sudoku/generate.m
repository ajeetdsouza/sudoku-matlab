function [unsolved, solved] = generate(subgridSize, nDrop)
% subgridSize - subgrid size of sudoku puzzle (typically 3)
% nDrop - approximate no. of values to remove from puzzle (can be less)
gridSize = subgridSize^2;

while true
    try
        puzzle = uint8(zeros(gridSize));
        for i = 1:subgridSize:gridSize
            puzzle(i:i+subgridSize-1, i:i+subgridSize-1) = reshape(randperm(gridSize), [subgridSize subgridSize]);
        end

        solved = sudoku.solve(puzzle);
        unsolved = solved;
        unsolved(randi([1 gridSize^2], 1, nDrop)) = 0;
        return
    catch
    end
end
end