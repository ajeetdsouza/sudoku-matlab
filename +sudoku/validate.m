function valid = validate(puzzle)
% Check dimensions and values of puzzle
gridSize = size(puzzle, 1);
subgridSize = sqrt(gridSize);

if ~ismatrix(puzzle) || ...                                   % puzzle is a 2D matrix
        ~(subgridSize == floor(subgridSize)) || ...           % gridSize is a perfect square
        ~(size(puzzle, 1) == size(puzzle, 2)) || ...          % puzzle is a square matrix
        ~(all(puzzle(:) >= 0) && all(puzzle(:) <= gridSize))  % puzzle has only permitted values
    valid = false;
    return
end

% Check rows and cols for duplicates
for i = 1:gridSize
    row = sort(nonzeros(puzzle(i, :)));
    col = sort(nonzeros(puzzle(:, i)));
    if ~isequal(row, unique(row)) || ~isequal(col, unique(col))
        valid = false;
        return
    end
end

% Check subgrids for duplicates
for row = 1:subgridSize:gridSize
    for col = 1:subgridSize:gridSize
        subgrid = sort(nonzeros(puzzle(row:row+subgridSize-1, col:col+subgridSize-1)));
        if ~isequal(subgrid, unique(subgrid))
            valid = false;
            return
        end
    end
end

valid = true;
return
end