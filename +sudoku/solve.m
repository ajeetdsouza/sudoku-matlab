function puzzle = solve(puzzle)
if ~sudoku.validate(puzzle)
    error('Error: Invalid puzzle.')
end

[puzzle, solved] = sudoku.solvers.backtrack(puzzle);
if ~solved
    error('Error: Unable to solve.')
end
end
