function [puzzle, solved] = backtrack(puzzle)
gridSize = size(puzzle, 1);

idx = find(~puzzle, 1);
if isempty(idx)
    solved = true;
    return
end

for i = 1:gridSize
    puzzle(idx) = i;
    if sudoku.validate(puzzle)
        [puzzleTemp, solved] = sudoku.solvers.backtrack(puzzle);
        if solved
            puzzle = puzzleTemp;
            return
        end
    end
end

solved = false;
end
