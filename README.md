# sudokulab
A simple Sudoku package for MATLAB, for educational purposes.

## Functions
- `board = sudoku.board(puzzle, 'title')` - displays a puzzle using `plot`.
- `puzzle = sudoku.solve(puzzle)` - solves a puzzle.
- `valid = validate(puzzle)` - checks if all values are within range, and that there are no conflicting values within rows, columns or subgrids. Does not check for empty fields, which can be done using `all(puzzle(:) > 0)`.
- `[solved, unsolved] = generate(3, 20)` - creates a Sudoku puzzle of subgrid size `3` (i.e. a 9x9 grid). The unsolved puzzle has upto `20` random cells left blank.
- `sudoku.save(puzzle, 'title', 'filename')` - saves a Sudoku puzzle as an image.
